require(lsa)
require(RTextTools)
require(plyr)
train <- read.delim("train.tsv")
train <- na.omit(train)
train$essay <- as.character(train$essay)
test <- read.delim("test.tsv")
test <- na.omit(test)
test$essay <- as.character(test$essay)

train$essay<-as.data.frame(train$essay)

###NEED YOUR HELP HERE:
#This doesn't work, because it expects file directory as its path. Can I somehow tell it to read different rows from the file?
### first version
trm <- textmatrix(train$essay,stemming=TRUE,language="english",minWordLength=2)
#This one works, but then I cannot use lsa(), as it says that it cannot allocate 2.4gb of memory.How can I deal with it?
### second version
trm <-create_matrix(train$essay, language="english",removeStopwords=TRUE, stemWords=TRUE, minWordLength=2)
space <- lsa(trm) # create LSA space
