setInternet2(TRUE)
# needed for finding files
require(stringr)
# needed for downloading site
require(RCurl)
# the URL you gave
baseSite <- "ftp://n4ftl01u.ecs.nasa.gov/SAN2/ICEBRIDGE/ILATM2.001"
# gets the listing of all folders in that base site
folders <- getURI(paste(baseSite, "/", sep=""))
# finds only folders with 2012
folders2012 <- str_extract_all(string=folders, pattern="2012\\.\\d{2}\\.\\d{2}")[[1]]
# puts a slash on the end
folders2012 <- paste(folders2012, "/", sep="")

# given a folder it searches for all files containing the pattern
list.files <- function(folder, pattern="smooth_nadir3seg")
{
    files <- getURI(folder)
    fullPattern <- sprintf("\\w+%s\\w+(\\.\\w+)*", pattern)
    files <- str_extract_all(string=files, pattern=fullPattern)[[1]]
    return(files)
}

# lists all files within a folder
theFiles <- list.files(file.path(baseSite, folders2012[1]))
# downloads one specific file from one specific folder
download.file(url=file.path(baseSite, folders2012[1], theFiles[1]), destfile=file.path("student code", "files", theFiles[1]), method="wget")