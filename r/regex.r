# needed for downloading data from wikipedia 
require(XML)
# needed for text manipulation
require(stringr)
# load plyr for colwise function
require(plyr)

# pull the data from the wiki page, we want the second table
wars <- readHTMLTable("http://en.wikipedia.org/wiki/List_of_wars_involving_the_United_States", which=2)

# Time is a factor
class(wars$Time)
# 5 columns
dim(wars)

# use colwise functions to run ```as.character``` on each column of wars
wars <- colwise(as.character)(wars)
# now Time is a character
class(wars$Time)

# split Time into a begin and end
# split on either "Ã¢Â€Â" or "-"
# this returns a list, each element is a vector of length 1 or 2
theTimes <- str_split(string=wars$Time, pattern="(Ã¢Â€Â)|-")
# go through the list and grab the first element of each vector
theStart <- sapply(theTimes, FUN=function(x) x[1])
# get rid of leading and trailing white space
theStart <- str_trim(theStart)

# pull out "January" anywhere it's found, otherwise return NA
str_extract(string=theStart, pattern="January")
# just return elements where "January" was detected
theStart[str_detect(string=theStart, pattern="January")]

# get the second through fifth caharacters
str_sub(string=theStart, start=2, end=5)
# get incidents of 4 numeric digits in a row
str_extract(string=theStart, "[0-9][0-9][0-9][0-9]")
# same as above but smarter
# "[0-9]" means any digits between 0 and 9
# "{4}" means it happens 4 times in a row
str_extract(string=theStart, "[0-9]{4}")
# smarter still
# "\\d" is a shortcut for "[0-9]"
str_extract(string=theStart, "\\d{4}")
# this looks for any digit that occurs either once, twice or thrice
str_extract(string=theStart, "\\d{1,3}")

# replace the first digit seen with "x"
str_replace(string=theStart, pattern="\\d", replacement="x")
# replace all digits seen with "x"
# this means "7" -> "x" and "382" -> "xxx"
str_replace_all(string=theStart, pattern="\\d", replacement="x")
# replace any strings of digits from 1 to 4 in length with "x"
# this means "7" -> "x" and "382" -> "x"
str_replace_all(string=theStart, pattern="\\d{1,4}", replacement="x")

# extract 4 digits at the begining of the text
str_extract(string=theStart, pattern="^\\d{4}")
# extract 4 digits at the end of the text
str_extract(string=theStart, pattern="\\d{4}$")
# extract 4 digits at the begining AND the end of the text
str_extract(string=theStart, pattern="^\\d{4}$")

# create a vector of sports
sports <- c("Hockey", "Ice Hockey", "Curling", "Field Hockey", "Football", "European Football", "Football Australian")
# replace "Hockey" with "Broomball", if a space is found insert that back (at "\\2") and if "Ice" is found insert that back too (at "\\1")
str_replace(string=sports, pattern="(Ice)*( )*Hockey", replacement="\\1\\2Broomball")

# build a string for us to search on
theString <- "<a href=index.html>The Link is here</a>"
# extract the ">", any text or spaces and "<"
str_extract(string=theString, pattern=">(\\w| )+<")
# extract the ">", anything (including spaces or numbers) and "<"
str_extract(string=theString, pattern=">.+?<")

# search for anything, then ">", then anything, then "<", then anything; replace with the middle anything
str_replace(string=theString, pattern="^.+?>(.+?)<.+$", replacement="\\1")
# similar to above but showing use of multiple replacements
str_replace(string=theString, pattern="^.+>(.+)? (.+)? .+?<.+$", replacement="\\1 hello \\2")

theString2 <- "<a href=index.html>The Link< is here</a>"
## notice the difference between the greedy and non-greedy search in handling that middle "<"
str_replace(string=theString2, pattern="^.+?>(.+)<.+$", replacement="\\1")
str_replace(string=theString2, pattern="^.+?>(.+?)<.+$", replacement="\\1")

# pattern to find anything between the bold and italic markers
"<i><b>(.+?)</b></i>"
# pattern for any number before a percent sign
"\\d*?\\%"

# digit between 0 and 9
"[0-9]"
# any capital letter
"[A-Z]"
# any lower case letter
"[a-z]"
# any letter
"[A-Za-z]"
# not any letter
"[^A-Za-z]"

"\\d" == "[0-9]"
"\\w" == "[A-Za-z]"
"\\s" == "any whitespace"

# help for regex searching
?regex