# let's build a simple function to see how it works
do.double <- function(x)
{
    return(x * 2)
}

# call the function
do.double(5)
# call it with a vector
do.double(c(7, 8, 846))
# does not support multiple objects
do.double(7, 8, 846)

# build a function with multiple arguments and default values
do.mult <- function(x, y=3)
{
    return(x * y)
}

# try calling it a few different ways
do.mult(7)
do.mult(4, 5)
do.mult(y=7, x=2)
do.mult(y=7, 2)

# function that passes a function as an argument
do.this <- function(x, func)
{
    do.call(func, args=list(x))
}
# call it
do.this(c(4, 5), mean)

# build a factor of sports
mySports <- factor(c("hockey", "lacrosse", "hockey", "Curling", "Cricket", "Bowling", "lacrosse", "water polo", "football", "Soccer", "football"))
model.matrix(~ mySports)

# load ggplot
require(ggplot2)
# get the diamonds data set
data(diamonds)
head(diamonds)

# fit a linear model on the diamonds dataset
mod1 <- lm(price ~ carat + cut, data=diamonds)
mod1
summary(mod1)

# extract the coeffcicients
theCoef <- coef(mod1)
# build a data.frame
coefDF <- data.frame(Coefficient=names(theCoef), Value=theCoef)
# make a simple dotplot
ggplot(coefDF, aes(x=Value, y=Coefficient)) + geom_point()

## build simple function to extract coefficients and plot
dotplot <- function(model, linecolor="black", linetype=2, dotcolor="blue")
{
    theCoef <- coef(model)
    coefDF <- data.frame(Coefficient=names(theCoef), Value=theCoef)
    ggplot(coefDF, aes(x=Value, y=Coefficient)) + 
        geom_vline(xintercept=0, color=linecolor, linetype=linetype) + 
        geom_point(colour=dotcolor)
}
# try it out
dotplot(mod1, linecolor="blue", dotcolor="chartreuse")


## a non-working skeleton for naive bayes
naive <- function(data, alpha=1, beta=1, class="Section")
{
    result <- ddply(data, .variables=class, .fun=naive.worker)
}

naive.worker <- function(data, alpha=1, beta=1, class="Section")
{
    if(nrow(data) == 0)
    {
        return(0)
    }else
    {
        print("hi")
    }
    
    data <- data[, names(data) != class]
    nc <- nrow(data)
    njc <- colSums(data)
}


## for loop examples
for(i in 1:10)
{
    if(i > 5)
    {
        break
    }
    print(i)
}

for(i in 1:10)
{
    if(i < 5)
    {
        next
    }
    print(i)
}

error <- mean((true - predicted)^2)

theFuncs <- function(x)
{
    c(man(x), sum(x), max(x))
}

theAssigns <- sample(nrow(diamonds))
training <- diamonds[theAssigns[1:20000], ]
testing <- diamonds[theAssigns[20001:nrow(diamonds)], ]
knn(train=training[, c("carat", "price")], test=