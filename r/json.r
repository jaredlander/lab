require(rjson)
require(plyr)

# the location of the dat
dataPath <- "http://getglue-data.s3.amazonaws.com/getglue_sample.tar.gz"

# build a connection that can decompress the file
theCon <- gzcon(url(dataPath))

# read 10 lines of the data
n.rows <- 10
theLines <- readLines(theCon, n=n.rows)

# check its structure
str(theLines)
# notice the first element is different than the rest
theLines[1]

# use fromJSON on each element of the vector, except the first
theRead <- lapply(theLines[-1], fromJSON)

# turn it all into a data.frame
theData <- ldply(theRead, as.data.frame)

# see how we did
View(theData)

