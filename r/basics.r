1 + 1

(2 + 3) * 7

3.74

5
5L

1:10.5

TRUE
FALSE

5 * TRUE
5 * FALSE

cities <- c("New York", "Boston", "New York", "Washington")
cities <- 6
cities
1:100
cities[1]
cities[2]

a <- 1:10
b <- -5:4
x = 1:10
1:10 -> x
x <- y <- 7:9
x
y
#
2 * 3 #+ 7


a <- 1:10
b <- -5:4
a
b

a <- a * 4

nchar("school")
nchar(cities)
nchar
lm

a + 4
a
b

a + b
a * b

x <- matrix(1:10, nrow=5)
y <- matrix(10:1, nrow=5)
x * y
z <- matrix(10:1, nrow=2)
z
x %*% z

2 == 3
2 != 3
2 < 3
2 > 3
2 <= 3

a
b
any(a > b)
all(a > b)
(x == y)


theData <- data.frame(A=a, J=b, Let=letters[1:10])
class(theData)
theData$J
class(theData[, 2, drop=FALSE])
class(theData[, 2])
theData[, "J"]
theData["J"]
theData[["J"]]

?system.time

plot(1:10, 1:10)

require(ggplot2)

data(diamonds)
head(diamonds)
ggplot(data=diamonds, aes(x=carat, y=price)) + geom_point()
ggplot(diamonds, aes(x=price)) + geom_histogram()

z
rm(z)
ls()

data()

read.table("http://stat.columbia.edu/~rachel/datasets/nyt1.csv", header=TRUE, 
           sep=",", stringsAsFactors=FALSE)

theFiles <- vector("list", 31)
for(i in 1:31)
{
    theName <- sprintf("http://stat.columbia.edu/~rachel/datasets/nyt%s.csv", 
                       i)
    theFiles[[i]] <- read.table(theName, header=TRUE, stringsAsFactors=FALSE)
}

require(plyr)
theData <- ldply(theFiles)

for(i in 1:31)
{
    theName <- sprintf("http://stat.columbia.edu/~rachel/datasets/nyt%s.csv", i)
    tempData <- read.table(theName, header=TRUE, stringsAsFactors=FALSE, sep=",")
    write.table(tempData, file=sprintf("data_%s.csv", i), row.names=FALSE, sep=",")
}

theData$age_group <- NA
theData$age_group <- ifelse(theData$Age < 18, "<18", theData$age_group)
theData$age_group <- ifelse(theData$Age >= 18 & theData$Age <= 24, "18-24", theData$age_group)
theData$age_group <- ifelse(theData$Age >= 25 & theData$Age <= 34, "25-34", theData$age_group)