# see what data is available 
data()
# what is LakeHuron like?
head(LakeHuron)
class(LakeHuron)
# EuStockMarkets?
class(EuStockMarkets)
# basixc plot of Lake Huron
plot(LakeHuron)

# how plot.ts is built
plot.ts
# plot multiple time series
plot(EuStockMarkets)

## let's build this plot in ggplot
# turn the multiple times series object into a data.frame
stocks <- as.data.frame(EuStockMarkets)
# first few rows
head(stocks)
# we need the reshape package to put the data into long format
require(reshape2)
# look at characteristics of EuStockMarkets
str(EuStockMarkets)
attr(EuStockMarkets, "tsp")
# build the date points
stocks$Date <- seq(from=1991.496, to=1998.646, by=1/260)
# we're not using it, but the lurbidate package makes handling darte objects much easier
require(lubridate)
# put the data into long form so it is easier to plot with ggplot
stockMelt <- reshape2:::melt.data.frame(data=stocks, id.vars="Date", 
                                        variable.name="Exchange", 
                                        value.name="Price")
head(stockMelt)
# load ggplot
require(ggplot2)
# create the ggplot object, plotting returns against date, faceting by exchange
g <- ggplot(stockMelt, aes(x=Date, y=Price, color=Exchange)) + geom_line() + facet_wrap(~Exchange, ncol=1)
g
# move the legend to the bottom
g + theme(legend.position="bottom")

## there's a new package that adds some great themes, but it's not on CRAN
## we need to get it from github
# first install and load the devtools package
install.packages("devtools")
require(devtools)
# now install the ggthemes package and load
install_github("ggthemes", "jrnold")
require(ggthemes)
# look like the Economist
g + theme_economist()
# look like excel
g + theme_excel2003()
# make Tufte proud
g + theme_tufte()

## plot autocorrelation and partial autocorrelation functions
acf(LakeHuron)
pacf(LakeHuron)

# load useful package and use it's plot function for time series data, only works on univariate time series for now
require(useful)
plot(LakeHuron)
# plot the acf and pacf with it
plot(LakeHuron, acf=TRUE)

# diffing time series can make it stationary and make modelling easier
lakeDiff <- diff(LakeHuron)
plot(lakeDiff)
acf(lakeDiff)
pacf(lakeDiff)

# look at arima requirements
?arima

#' @title fit.ts
#' @name Time Series Fitting
#' @details This function is used for fitting many different arima models
#' @param ts the time series data
#' @param ar The possible autoregressive orders for the model
#' @param int The possible integrated (diffing) orders for the model
#' @param ma The possible moving average orders for the model
#' @return A list holding a model for each combination of ar, int and ma
fit.ts <- function(ts, ar=0:2, int=0:1, ma=0:2)
{
    # build every possible combination of ar, int and ma
    theGrid <- expand.grid(ar, int, ma)
    
    # create an empty list to hold the results
    results <- vector(mode="list", length=nrow(theGrid))
    
    ## loop through the possible combinations and fit the model with those orders
    for(i in 1:nrow(theGrid))
    {
        results[[i]] <- arima(x=ts, order=unlist(theGrid[i, ]))
    }
    
    return(results)
}

# run the function
theModels <- fit.ts(LakeHuron)
# inspect a particular model
theModels[[2]]
# load the plyr package
require(plyr)
# calculate the AIC for each model
theAICs <- laply(theModels, AIC)
# the smallest AIC
theAICs[which.min(theAICs)]
# the model with smallest AIC
theModels[[which.min(theAICs)]]

# how to convert object to a time series
?as.ts

# making a Date
as.Date("10-08-2012", format="%m-%d-%Y")
as.Date(16637)

# other date types, avoid POSIXlt
?POSIXct
?POSIXlt

# load diamonds data from ggplot package
data(diamonds)
## fit a few models
mod1 <- lm(price ~ carat + cut + color, data=diamonds)
mod2 <- lm(price ~ carat + cut + color + clarity, data=diamonds)
# compare models using anova, not recommended
anova(mod1, mod2)
# examine one model
summary(mod1)
# load coefplot package
require(coefplot)
# view coefficients
coefplot(mod1)
coefplot(mod1) + xlim(-100, 1000)

# package for variable selection
require(leaps)
# package for cross validation
require(cvTools)

# load glmnet package for fitting elasticnet (lasso.ridge regression)
require(glmnet)
# build response matrix
diaY <- as.matrix(diamonds$price)
# build predictor matrix
diaX <- model.matrix(~carat + cut + color + clarity, data=diamonds)
head(diaX)

# fit the glmnet
mod3 <- glmnet(x=diaX, y=diaY)
?glmnet
plot(mod3)

# use cross validation to pick optimal lambda
mod4 <- cv.glmnet(x=diaX, y=diaY)
# view plot of lambas abd the resulting MSE
plot(mod4)

# understand the role of parenthesis when building a range of numbers
1:(10-1)
1:10-1
# see how seq works
seq(from=0, to=20, by=2)
seq.Date(from=as.Date("1997-04-25"), to=as.Date("1999-06-27"), by="1 month")
seq.Date(from=as.Date("1997-04-25"), to=as.Date("1999-06-27"), by="2 month")
seq.Date(from=as.Date("1997-04-25"), to=as.Date("1999-06-27"), by="1 week")

## back to LakeHuron, let's build a data.frame so we can examine how arima works
head(LakeHuron)
# create a data.frame with one column for the data and a second column for the data shifted backward by one
thisTS <- data.frame(Response=LakeHuron[2:length(LakeHuron)], 
                     Predictor=LakeHuron[1:(length(LakeHuron)-1)])

# use the function useful::shift.column to do the same as above
thatTS <- shift.column(data=thisTS, columns="Predictor")
# fit and visualize the model with one lag
coefplot(lm(Response ~ Predictor, data=thisTS))
# fit and visualize the model with two lags
coefplot(lm(Response ~ Predictor + Predictor.Shifted, data=thatTS))

# try this with dynlm
require(dynlm)
?dynlm

# other useful time series packages
require(zoo)
require(xts)
require(tseries)