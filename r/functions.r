#
do.double <- function(x, y=3)
{
    return(x*y)
}

system.time({result <- do.double(4, 6)})

mult.args <- function(x, y)
{
    do.double(x, y)
}

mult.args(3, 5)
traceback()

require(ggplot2)
data(diamonds)
head(diamonds)
class(diamonds$cut)
myChar <- as.factor(c("Hockey", "Lacrosse", "Hockey", "Soccer", "Cricket", "Frolf", "Lacrosse", "Curling"))
model.matrix(~myChar)
head(model.matrix(~ color, data=diamonds))
myDF <- data.frame(A=1:8, Sport=myChar)
myDF
model.matrix(~ A + Sport - 1, data=myDF)
mod1 <- lm(price ~ carat + cut, data=diamonds)
summary(mod1)

mod1$coefficients
theCoef <- coef(mod1)
class(theCoef)
coefDF <- data.frame(Variable=names(theCoef), Value=theCoef)

ggplot(coefDF, aes(x=Value, y=Variable)) + geom_point()

dotplot <- function(model, linecolor="black", linetype=2, dotcolor="blue")
{
    # grab the coefficients
    theCoef <- coef(model)
    # turn into a data.frame
    coefDF <- data.frame(Variable=names(theCoef), Value=theCoef)
    # plot
    ggplot(coefDF, aes(x=Value, y=Variable)) + geom_point(colour=dotcolor) +
        geom_vline(xintercept=0, colour=linecolor, linetype=linetype)
}
dotplot(mod1, linecolor="red", dotcolor="purple")
g <- dotplot(mod1)
class(g)
str(g)
x <- 4

ggplot(diamonds, aes(x=carat, y=price)) + geom_point()
head(mod1$fitted.values)
diamonds$fitted <- mod1$fitted.values
ggplot(diamonds, aes(x=carat, y=price)) + geom_point() + geom_line(aes(y=fitted), colour="blue")

naive <- function(data, alpha=1, beta=1, class="Section")
{
    result <- ddply(data, .variables=class, .fun=naive.worker, alpha=alpha, beta=beta)
    sum(result)
}
naive.worker <- function(data, alpha=1, beta=1, class)
{
    data <- data[, which(names(data) == class)]
    njc <- colSums(data)
    nc <- nrow(data)
}

newFunc <- function(arg)
{
    arg * x
}
newFunc(7, 9)
newFunc(7)
rm(x)
x
rm(list=ls())