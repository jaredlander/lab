# matrices
import numpy as npy

arr1 = np.array([[1, 2], [3, 4]])
arr2 = np.array([[5, 6], [7, 8]])
arr3 = np.zeros((2, 2))

for i in range(2):
    for j in range(2):
        arr3[i, j] = arr1[i, j] * arr2[i, j]

arr1 * arr2
arr1 * arr2 == arr3
np.all(arr1 * arr2 == arr3)

arr1 = np.arange(4)
arr1.shape = (2, 2)
arr2 = np.arange(4, 8)
arr2.shape = (2, 2)
print arr1
print arr2

arr1.dot(arr2)

vec = np.array([1, 2, 3])
vec
arr1.dot(vec)
arr1.dot(vec[:2])

# nobody uses matrix
mat1 = np.matrix(arr1)
mat2 = np.matrix(arr2)
mat1 * mat2


arr = np.arange(10)

arr[arr > 3]
arr[(arr > 3) & (arr < 8)]

mask = [1, 4, 8]
arr[mask]

