# non-neg average

## making a function
def average_positive(entries):
    tempSum = 0
    counter = 0
    
    for a in entries:
        if a > 0:
            tempSum += a
            counter += 1
    assert counter > 0, "No positive ages provided"
    return float(tempSum) / counter
    
def running_sum(data):
    assert len(data) > 0, "Must specify some numbers"
    result = []
    current = data[0]
    result.append(current)
    for value in data[1:]:
        current = current + value
        result.append(current)
    return result

def running_product(data):
    assert len(data) > 0, "Must specify some numbers"
    result = []
    current = data[0]
    result.append(current)
    for value in data[1:]:
        current = current * value
        result.append(current)
    return result

def add(a, b):
    return a + b
    
def mul(a, b):
    return a * b
    
def running(what_to_do, data):
    assert len(data) > 0, "Must specify some numbers"
    result = []
    current = data[0]
    result.append(current)
    for value in data[1:]:
        current = what_to_do(current, value)
        result.append(current)
    return result