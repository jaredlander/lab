## using numpy
import numpy as np      # import numpy library, rename np

A = np.arange(1000000)
B = np.arange(1000000)
C = np.arange(1000000)

%timeit Z = A + (B * C)

import matplotlib
ax = subplot(111)
x = np.arange(0.0, 5.0, 0.01)
y = np.cos(x * np.pi)
#print y
lines, = plot(x, y, lw=3)
ylim(-1.5, 1.5)
show()


vals = [1, 2, 3]
arr = np.array(vals)
arr

np.array

def myFunc(a, b, myDefault=1):
    print a, b, myDefault

print np.array([1, 2, 3, 4], dtype=np.int)
print np.array([1, 2, 3, 4], dtype=np.float)
print np.array([1, 2, 3, 4], dtype=np.complex)

arr.dtype
arr2 = np.array(arr, dtype="float64")
arr3 = np.array(arr, dtype="int64")
arr3

z = np.zeros((2, 3), dtype=np.float64)

np.identity(2)


arr = np.linspace(0, 1, 20)
arr = np.logspace(0, 1, 20, base=10)

first = np.ones((2, 2))
print first
second = first.copy()
second[0, 0] = 9

arr = first
arr
arr.shape
arr.shape = [4, 1]

arr = np.array([[1, 2], [3, 4]])
arr.T
arr.transpose()