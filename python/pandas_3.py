# pandas group by
import numpy as np
import pandas as pd
import pandas.io.data as web
from pandas import DataFrame

def pct_change(x):
    return x / x.shift(1) - 1
    
stocks = ['AAPL', 'FB', 'GOOG']

st = '2012-6-1'
ed = '2012-9-29'

dd = {}

for s in stocks:
    dd[s] = web.get_data_yahoo(s, st, ed)['Adj Close']

df = DataFrame(dd)
df.head()
returns = df.pct_change()

df.groupby(mapper).apply(zscore)

small = df.AAPL[0:5]
mapper = [0, 0, 1, 1, 1]
small.groupby(mapper).mean()
(558.59+561.88)/2
(560.42+569.02+569.28)/3

months = Series(df.index.month, df.index)

def running_product(data):
    assert len(data) > 0, "Must specify some numbers"
    result = []
    current = data[0]
    result.append(current)
    for value in data[1:]:
        current = current * value
        result.append(current)
    return result

def cumulative_daily(returns):
    # cumulative daily returns
    result = []
    current = returns[0]
    result.append(current)
    for ret in returns[1:]:
        current += + ret
        result.append(current)
    return result
cumulative_daily(df.GOOG)
    
    
def cumulative_monthly(returns, grouper):
    # cumulative monthly returns
    # for each month, compute compounded daily return
    # compute the cumulative returns of all the months
    bigSum = returns.groupby(grouper).sum()
    return cumulative_daily(bigSum)

# def cumulative_sum(n):
    # cum_sum = []
    # y = 0
    # for i in n:
        # y += i
        # cum_sum.append(y)
    # return(cum_sum)