## recommendation engine
import nump as np

# load data
%load https://raw.github.com/iguananaut/notebooks/class/exercises/recommend_data.py

names = ['Bhargan Basepair', 'Fan Fullerene', 'Gail Graphics',
         'Helen Helmet', 'Mehrdad Mapping', 'Miguel Monopole',
         'Stephen Scanner']

papers = ['Chen 2002', 'Chen 2008', 'El Awy 2005', 'Falkirk et al 2006',
          'Jackson 1999', 'Rollins and Khersau 2002']

ratings = np.array(
    [[ 3.5,  2.5,  3.5,  3. ,  2.5,  3. ],
     [ 3.5,  3.5,  5. ,  3. ,  3. ,  1.5],
     [ 4. ,  3.5,  5. ,  3. ,  3. ,  0. ],
     [ 3. ,  0. ,  3.5,  4. ,  2.5,  0. ],
     [ 3.5,  2.5,  4. ,  4.5,  0. ,  3. ],
     [ 4. ,  2. ,  3. ,  3. ,  3. ,  2. ],
     [ 4.5,  1. ,  4. ,  0. ,  0. ,  0. ]])
     

## top match function     
def top_matches(ratings, person, num, similarity_func):
    scores = []
    for other in xrange(ratings.shape[0]):
        if other != person:
            scores.append((similarity_func(ratings, person, other), other))
    scores.sort()
    scores.reverse()
    return scores[0:num]
    
    
def similarity_distance(ratings, left_index, right_index):
    """computes similarity matrix
    """
    left_ratings = ratings[left_index]
    right_ratings = ratings[right_index]
    
    left_has_ratings = left_ratings > 0
    right_has_ratings = right_ratings > 0
    
    mask = left_has_ratings & right_has_ratings
    
    diff = left_ratings[mask] - right_ratings[mask]
    
    sum_of_squares = np.sum(diff ** 2) ** 2
    #sum_of_squares = (diff ** 2).sum()
    
    return 1 / (1 + sum_of_squares)

    
top_matches(ratings, 0, 2, similarity_distance)

ratings_test = np.array([[0, 1], [1, 2], [3, 0]])
top_matches(ratings_test, 0, 1, similarity_distance)