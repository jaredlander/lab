## indexing
import numpy as np

arr = np.array([[1, 2], [3, 4]])
arr
arr[0]
arr[0][0]
arr[0][1]
arr[0, 0]
arr[0, 1]
arr[1, 1]
arr[:, 1]

arr = np.array([[0, 1], [2, 3], [4, 5]])
arr = np.arange(0, 6)
arr.shape = (3, 2)
arr

arr = np.arange(9)
arr.shape = (3, 3)
arr[0:2]
arr[0:2, 0:2]


arr2 = arr[0:1, 0:1]


