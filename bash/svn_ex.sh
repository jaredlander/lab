# grab repository on first run
svn checkout http://svn.software-carpentry.org/columbia --username jaredlander --password jaredPassword

# see what you changed or added
svn status

# add file to tracking
svn add greg.txt

# commit the file
svn commit -m "Here is my message."

# pull changes
svn update

# remove a file from tracking and in actuality
svn remove thisfile.txt

# whoe wrote last line
svn blame thatfile.txt

# see who added/deleted what lines
scv log thatfile.txt

# mark file as resolved
svn resolved thatfile.txt

# revert
svn revert thatfile.txt

# diff
svn diff thatfile.txt

svn propset svn:keywords Revision theFile.txt

#### file comments

# $Revision:$
